/*
Spawner - Gather spawners with silk touch enchanted tools. Provides the ability to change mob types and other features.

The MIT License (MIT)

Copyright (c) 2022 Ryan Rhode

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package me.ryvix.spawner;

import com.google.gson.Gson;
import de.tr7zw.nbtapi.*;
import de.tr7zw.nbtapi.iface.ReadWriteNBT;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

// TODO: show SpawnPotentials data in item lore
public class SpawnerHandler implements me.ryvix.spawner.api.Spawner {

	public void defaultBlockEntityTag(NBTCompound compound) {
		NBTCompound blockEntityTag = compound.addCompound("BlockEntityTag");

		blockEntityTag.setShort("MaxNearbyEntities", (short) 6);
		blockEntityTag.setShort("RequiredPlayerRange", (short) 16);
		blockEntityTag.setShort("SpawnCount", (short) 4);
		blockEntityTag.setShort("SpawnRange", (short) 4);
		blockEntityTag.setShort("Delay", (short) -1);
		blockEntityTag.setShort("MinSpawnDelay", (short) 200);
		blockEntityTag.setShort("MaxSpawnDelay", (short) 800);

		blockEntityTag.setString("id", "minecraft:mob_spawner");

		defaultSpawnData(blockEntityTag);
		defaultSpawnPotentials(blockEntityTag);

		compound.getCompound("BlockEntityTag").mergeCompound(blockEntityTag);

	}

	public void defaultSpawnData(NBTCompound compound) {
		NBTCompound spawnData = compound.addCompound("SpawnData");

		defaultEntity(spawnData);

		compound.getCompound("SpawnData").mergeCompound(spawnData);

	}

	public void defaultSpawnPotentials(NBTCompound compound) {

		//compound.mergeCompound(new NBTContainer("{tag:{BlockEntityTag:{MaxNearbyEntities:6s,RequiredPlayerRange:16s,SpawnCount:4s,SpawnData:{entity:{id:\"minecraft:pig\"}},id:\"minecraft:mob_spawner\",MaxSpawnDelay:800s,SpawnRange:4s,Delay:-1s,SpawnPotentials:[{data:{entity:{id:\"minecraft:pig\"}},weight:1}],MinSpawnDelay:200s}}}"));

		NBTCompoundList spawnPotentials = compound.getCompoundList("SpawnPotentials");

		NBTCompound spawnPotential = spawnPotentials.addCompound(null);
		NBTCompound data = spawnPotential.addCompound("data");
		defaultEntity(data);
		spawnPotential.getCompound("data").mergeCompound(data);

		NBTCompoundList newSpawnPotentials = compound.getCompoundList("SpawnPotentials");

		for (ReadWriteNBT newSpawnPotential : spawnPotentials) {
			newSpawnPotentials.addCompound(newSpawnPotential);
		}

		spawnPotential.setInteger("weight", 1);

	}

	public void defaultEntity(NBTCompound compound) {
		NBTCompound entity = compound.addCompound("entity");

		entity.setString("id", "minecraft:pig");

		compound.getCompound("entity").mergeCompound(entity);

	}

	/**
	 * Converts any relevant PersistentData tags to NBT data
	 *
	 * @param compound NBTCompound
	 * @return compound NBTCompound
	 */
	public NBTCompound convertPersistentDataToNBT(NBTCompound compound) {
		if (compound.hasTag("PublicBukkitValues")) {
			/*
			{
				PublicBukkitValues: {
					"spawner:tag": {
						"spawner:blockentitytag": {
							"spawner:maxspawndelay": 800s,
							"spawner:minspawndelay": 200s,
							"spawner:spawnrange": 4s,
							"spawner:maxnearbyentities": 6s,
							"spawner:spawndata": {
								"spawner:id": "pig"
							},
							"spawner:spawnpotentials": [
								{
									"spawner:weight": 1,
									"spawner:entity": {
										"spawner:id": "pig"
									}
								}
							],
							"spawner:requiredplayerrange": 16s,
							"spawner:delay": -1s,
							"spawner:spawncount": 4s
						}
					}
				},
				display: {
					Name: '{"extra":[{"bold":false,"italic":false,"underlined":false,"strikethrough":false,"obfuscated":false,"color":"yellow","text":"pig "},{"bold":false,"italic":false,"underlined":false,"strikethrough":false,"obfuscated":false,"color":"green","text":"spawner"}],"text":""}'
				}
			}
			 */

			NBTCompound blockEntityTag;
			if (!compound.hasTag("BlockEntityTag")) {
				defaultBlockEntityTag(compound);
			}
			blockEntityTag = compound.getCompound("BlockEntityTag");

			NBTCompound publicBukkitValues = compound.getCompound("PublicBukkitValues");
			if (publicBukkitValues.hasTag("spawner:tag")) {
				NBTCompound spawnerTag = publicBukkitValues.getCompound("spawner:tag");

				if (spawnerTag.hasTag("spawner:blockentitytag")) {
					NBTCompound spawnerBlockEntityTag = spawnerTag.getCompound("spawner:blockentitytag");

					if (spawnerBlockEntityTag.hasTag("spawner:maxspawndelay")) {
						blockEntityTag.setShort("MaxSpawnDelay", spawnerBlockEntityTag.getShort("spawner:maxspawndelay"));
					} else {
						blockEntityTag.setShort("MaxSpawnDelay", (short) 800);
					}

					if (spawnerBlockEntityTag.hasTag("spawner:minspawndelay")) {
						blockEntityTag.setShort("MinSpawnDelay", spawnerBlockEntityTag.getShort("spawner:minspawndelay"));
					} else {
						blockEntityTag.setShort("MinSpawnDelay", (short) 200);
					}

					if (spawnerBlockEntityTag.hasTag("spawner:spawnrange")) {
						blockEntityTag.setShort("SpawnRange", spawnerBlockEntityTag.getShort("spawner:spawnrange"));
					} else {
						blockEntityTag.setShort("SpawnRange", (short) 4);
					}

					if (spawnerBlockEntityTag.hasTag("spawner:maxnearbyentities")) {
						blockEntityTag.setShort("MaxNearbyEntities", spawnerBlockEntityTag.getShort("spawner:maxnearbyentities"));
					} else {
						blockEntityTag.setShort("MaxNearbyEntities", (short) 6);
					}

					if (spawnerBlockEntityTag.hasTag("spawner:requiredplayerrange")) {
						blockEntityTag.setShort("RequiredPlayerRange", spawnerBlockEntityTag.getShort("spawner:requiredplayerrange"));
					} else {
						blockEntityTag.setShort("RequiredPlayerRange", (short) 16);
					}

					if (spawnerBlockEntityTag.hasTag("spawner:delay")) {
						blockEntityTag.setShort("Delay", spawnerBlockEntityTag.getShort("spawner:delay"));
					} else {
						blockEntityTag.setShort("Delay", (short) 16);
					}

					if (spawnerBlockEntityTag.hasTag("spawner:spawncount")) {
						blockEntityTag.setShort("SpawnCount", spawnerBlockEntityTag.getShort("spawner:spawncount"));
					} else {
						blockEntityTag.setShort("SpawnCount", (short) 4);
					}

					if (spawnerBlockEntityTag.hasTag("spawner:spawndata")) {
						NBTCompound spawnerSpawnData = spawnerBlockEntityTag.getCompound("spawner:spawndata");

						String spawnerId = "minecraft:pig";
						if (spawnerSpawnData.hasTag("spawner:id")) {
							spawnerId = "minecraft:" + spawnerSpawnData.getString("spawner:id");
						}

						NBTCompound spawnData = spawnerBlockEntityTag.addCompound("SpawnData");
						spawnData.addCompound("entity").setString("id", spawnerId);

						blockEntityTag.addCompound("SpawnData").mergeCompound(spawnData);

					} else {
						defaultSpawnData(blockEntityTag);
					}

					if (spawnerBlockEntityTag.hasTag("spawner:spawnpotentials")) {

						blockEntityTag.removeKey("SpawnPotentials");
						NBTCompoundList spawnPotentials = blockEntityTag.getCompoundList("SpawnPotentials");

						NBTCompoundList spawnerSpawnPotentialsList = spawnerBlockEntityTag.getCompoundList("spawner:spawnpotentials");
						AtomicInteger i = new AtomicInteger();
						spawnerSpawnPotentialsList.forEach(nbtListCompound -> {
							NBTListCompound spawnPotential = spawnPotentials.addCompound();

							if (nbtListCompound.hasTag("spawner:entity")) {
								NBTCompound spawnerEntity = (NBTCompound) nbtListCompound.getCompound("spawner:entity");
								if (spawnerEntity.hasTag("spawner:id")) {
									spawnPotential.addCompound("data").addCompound("entity").setString("id", "minecraft:" + spawnerEntity.getString("spawner:id"));
								} else {
									spawnPotential.addCompound("data").addCompound("entity").setString("id", "minecraft:pig");
								}
							}
							if (nbtListCompound.hasTag("spawner:weight")) {
								spawnPotential.setInteger("weight", nbtListCompound.getInteger("spawner:weight"));
							} else {
								spawnPotential.setInteger("weight", 1);
							}

							blockEntityTag.getCompoundList("SpawnPotentials").get(i.get()).mergeCompound(spawnPotential);

							i.getAndIncrement();
						});

						compound.getCompound("BlockEntityTag").mergeCompound(blockEntityTag);

					} else {
						defaultSpawnPotentials(blockEntityTag);
					}
				}
			}

//			compound.removeKey("PublicBukkitValues");
		}

		return compound;
	}

	/**
	 * Set minimal default data.
	 *
	 * @param spawner Spawner
	 * @param name    String
	 * @return Spawner
	 */
	@Override
	public Spawner setDefault(Spawner spawner, String name) {

		ItemMeta meta = spawner.getItemMeta();
		if (meta == null) {
			return spawner;
		}

		/*
		Item data structure:

		tag
		|-BlockEntityTag
			|-SpawnData
				|-id:minecraft:{ENTITY}
			|-SpawnPotentials
				|-Entity
					|-id:minecraft:{ENTITY}
				|-Weight:1
			|-Delay:-1
			|-id:minecraft:mob_spawner
			|-MaxNearbyEntities:6
			|-MaxSpawnDelay:800
			|-MinSpawnDelay:200
			|-RequiredPlayerRange:16
			|-SpawnCount:4
			|-SpawnRange:4
			display
			|-Name:JSON encoded data
		Count:1
		id:minecraft:spawner
		Slot:3

		 */

		// Use NBT API plugin
		// https://www.spigotmc.org/resources/nbt-api.7939/
		NBTItem spawnerNBTItem = new NBTItem(spawner);
		//spawnerNBTItem.mergeCompound(new NBTContainer("{tag:{BlockEntityTag:{MaxNearbyEntities:6s,RequiredPlayerRange:16s,SpawnCount:4s,SpawnData:{entity:{id:\"minecraft:pig\"}},id:\"minecraft:mob_spawner\",MaxSpawnDelay:800s,SpawnRange:4s,Delay:-1s,SpawnPotentials:[{data:{entity:{id:\"minecraft:pig\"}},weight:1}],MinSpawnDelay:200s}}}"));

		defaultBlockEntityTag(spawnerNBTItem);

		spawner.setItemMeta(spawnerNBTItem.getItem().getItemMeta());

		return spawner;
	}

	/**
	 * Set Block data from Spawner item.
	 *
	 * @param spawner Spawner
	 * @param block   Block
	 * @return boolean
	 */
	@Override
	public boolean setSpawnerBlock(Spawner spawner, Block block) {
		if (!(spawner.getType() == Material.SPAWNER)) {
			return false;
		}

		ItemMeta meta = spawner.getItemMeta();
		if (meta == null) {
			return false;
		}

		// Make sure the spawner has data set. The spawner should always have a name at this point.
		String name = spawner.getEntityName();
		spawner = setSpawner(spawner, name);

		/*
		TileEntities data structure:

			|-SpawnData
				|-id:minecraft:{ENTITY}
			|-SpawnPotentials
				|-Entity
					|-id:minecraft:{ENTITY}
				|-Weight:1
			|-Delay:-1
			|-id:minecraft:mob_spawner
			|-keepPacked:0
			|-MaxNearbyEntities:6
			|-MaxSpawnDelay:800
			|-MinSpawnDelay:200
			|-RequiredPlayerRange:16
			|-SpawnCount:4
			|-SpawnRange:4
			|-x:{INT}
			|-y:{INT}
			|-z:{INT}

		 */

		NBTItem spawnerNBTItem = new NBTItem(spawner);

		BlockState blockState = block.getState();

		NBTCompound tileEntityCompound = new NBTTileEntity(block.getState());

		// BlockEntityTag
		NBTCompound blockEntityTag;
		if (spawnerNBTItem.hasTag("BlockEntityTag")) {
			blockEntityTag = spawnerNBTItem.getCompound("BlockEntityTag");
		} else if (!spawnerNBTItem.hasTag("BlockEntityTag") && spawnerNBTItem.hasTag("PublicBukkitValues")) {
			NBTCompound tag = convertPersistentDataToNBT(spawnerNBTItem);
			spawnerNBTItem.mergeCompound(tag);
			blockEntityTag = spawnerNBTItem.getCompound("BlockEntityTag");

			// Get BlockEntityTag after conversion
			if (!spawnerNBTItem.hasTag("BlockEntityTag")) {
				defaultBlockEntityTag(spawnerNBTItem);
			}

		} else {
			defaultBlockEntityTag(spawnerNBTItem);
			blockEntityTag = spawnerNBTItem.getCompound("BlockEntityTag");
		}

		tileEntityCompound.mergeCompound(blockEntityTag);

		// SpawnPotentials
		if (!tileEntityCompound.hasTag("SpawnPotentials")) {
			if (!blockEntityTag.hasTag("SpawnPotentials")) {
				defaultSpawnPotentials(blockEntityTag);
			}
			tileEntityCompound.removeKey("SpawnPotentials");
			NBTListCompound spawnPotential = tileEntityCompound.getCompoundList("SpawnPotentials").addCompound();
			spawnPotential.addCompound("data").addCompound("entity").setString("id", "minecraft:" + name);
			spawnPotential.setInteger("weight", 1);
		}

		// SpawnData
		if (!tileEntityCompound.hasTag("SpawnData")) {
			if (!blockEntityTag.hasTag("SpawnData")) {
				defaultSpawnData(blockEntityTag);
			}
			tileEntityCompound.removeKey("SpawnData");
			tileEntityCompound.addCompound("SpawnData").mergeCompound(blockEntityTag.getCompound("SpawnData"));
		}

		blockState.update(true);

		return true;
	}

	/**
	 * Set spawner type
	 *
	 * @param spawner Spawner
	 * @return CraftItemStack
	 * @see <a href="https://minecraft.fandom.com/wiki/Spawner#Block_data">https://minecraft.fandom.com/wiki/Spawner#Block_data</a>
	 */
	@Override
	public Spawner setSpawner(Spawner spawner, String name) {
		if (!(spawner.getType() == Material.SPAWNER)) {
			return null;
		}

		// Gets entity name from spawner using various methods and then gets a clean version using the language key.
		String cleanEntity = Main.instance.getLangHandler().translateEntity(name.toLowerCase(), "key");

		// The name variable should be a valid entity at this point so use it as the default if necessary.
		//if (cleanEntity.equals("default")) {
		//String cleanEntity = name.toLowerCase();
		//}

		// Set default data.
		spawner = setDefault(spawner, cleanEntity);

		// Set spawner name.
		spawner.setFormattedName(name);

		// Set spawner data.
		setSpawnerEntity(spawner, name);

		// TODO: convert tag section to JSON format
		// Set data from config for each spawner type if necessary.
		// entities.yml entities.<entity>.tag
		ConfigurationSection tagSection = (ConfigurationSection) Main.instance.getConfigHandler().getConfigValue("tag", cleanEntity, "ConfigurationSection");
		if (tagSection != null) {

			ItemMeta meta = spawner.getItemMeta();
			if (meta == null) {
				return spawner;
			}

			NBTItem spawnerNBTItem = new NBTItem(spawner);

			NBTCompound blockEntityTag;
			if (spawnerNBTItem.hasTag("BlockEntityTag")) {
				blockEntityTag = spawnerNBTItem.getCompound("BlockEntityTag");
			} else if (!spawnerNBTItem.hasTag("BlockEntityTag") && spawnerNBTItem.hasTag("PublicBukkitValues")) {
				NBTCompound tag = convertPersistentDataToNBT(spawnerNBTItem);
				spawnerNBTItem.mergeCompound(tag);
				blockEntityTag = spawnerNBTItem.getCompound("BlockEntityTag");

				// Spawner is returned in this function, so save its data.
				spawner.setItemMeta(spawnerNBTItem.getItem().getItemMeta());

				// Get BlockEntityTag after conversion
				if (!spawnerNBTItem.hasTag("BlockEntityTag")) {
					defaultBlockEntityTag(spawnerNBTItem);
				}
			} else {
				defaultBlockEntityTag(spawnerNBTItem);
				blockEntityTag = spawnerNBTItem.getCompound("BlockEntityTag");
			}

			// entities.yml entities.<entity>.tag.blockentitytag
			if (tagSection.contains("blockentitytag")) {

				// entities.yml entities.<entity>.tag.spawndata.id
				String tag_spawndata_id_value = "minecraft:" + cleanEntity;
				if (tagSection.contains("spawndata.id") && !Objects.equals(tagSection.getString("spawndata.id"), "")) {

					String tag_spawndata_id = tagSection.getString("spawndata.id");

					// Validate Spawndata.id
					assert tag_spawndata_id != null;
					if (!tag_spawndata_id.isEmpty()) {
						tag_spawndata_id_value = tag_spawndata_id;
					}
				}

//				if (tag == null || blockEntityTag == null) {
//					return spawner;
//				}

				// SpawnData (overwritten after next spawn attempt if SpawnPotentials is set)
				NBTCompound iSpawnData;
				assert blockEntityTag != null;
				if (!blockEntityTag.hasTag("SpawnData")) {
					defaultSpawnData(blockEntityTag);
				}
				iSpawnData = blockEntityTag.getCompound("SpawnData");

				// Spawndata.entity.id
				iSpawnData.addCompound("entity").setString("id", tag_spawndata_id_value);

				// Save SpawnData to NBT tag.
				blockEntityTag.getCompound("SpawnData").mergeCompound(iSpawnData);

				// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials
				if (tagSection.contains("blockentitytag.spawnpotentials")) {
					ConfigurationSection spawnPotentials = tagSection.getConfigurationSection("blockentitytag.spawnpotentials");
					assert spawnPotentials != null;
					Set<String> spawnPotentialsSet = spawnPotentials.getKeys(false);

					if (!blockEntityTag.hasTag("SpawnPotentials")) {
						defaultSpawnPotentials(blockEntityTag);
					}

					// Clear any existing SpawnPotentials
					blockEntityTag.getCompoundList("SpawnPotentials").clear();

					// Loop through each spawnpotential # set for this entity in entities.yml
					for (String spawnPotentialKey : spawnPotentialsSet) {
						NBTCompound spawnPotentialsEntry = new NBTContainer();

						String spawnPotentialPath = "blockentitytag.spawnpotentials." + spawnPotentialKey + ".";

						// TODO: add support for more tags

						// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials.#.entity.id
						// Note: Overwrites SpawnData and SpawnData.id
						if (tagSection.contains(spawnPotentialPath + "entity.id") && !Objects.equals(tagSection.getString(spawnPotentialPath + "entity.id"), "")) {
							String tag_blockentitytag_spawnpotentials_entity_id = tagSection.getString(spawnPotentialPath + "entity.id");

							// Validate entity type.
							if (SpawnerFunctions.getEntities(false, false).contains(tag_blockentitytag_spawnpotentials_entity_id)) {

								// NBT BlockEntityTag.SpawnPotentials.#.Entity
								NBTCompound spawnPotentialsEntity = spawnPotentialsEntry.getOrCreateCompound("data").getOrCreateCompound("entity");
								spawnPotentialsEntity.setString("id", "minecraft:" + tag_blockentitytag_spawnpotentials_entity_id);

								// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials.#.invulnerable
								if (tagSection.contains(spawnPotentialPath + "invulnerable")) {
									boolean tag_blockentitytag_spawnpotentials_invulnerable = tagSection.getBoolean(spawnPotentialPath + "invulnerable");

									// NBT BlockEntityTag.SpawnPotentials.#.Entity.Invulnerable
									spawnPotentialsEntity.setBoolean("Invulnerable", tag_blockentitytag_spawnpotentials_invulnerable);
								}

								// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials.#.customname
								if (tagSection.contains(spawnPotentialPath + "customname")) {
									String tag_blockentitytag_spawnpotentials_customname = Main.instance.getLangHandler().colorText(tagSection.getString(spawnPotentialPath + "customname"));


									// NBT BlockEntityTag.SpawnPotentials.#.Entity.CustomName
									spawnPotentialsEntity.setString("CustomName", new Gson().toJson(tag_blockentitytag_spawnpotentials_customname));
								}

								// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials.#.customnamevisible
								if (tagSection.contains(spawnPotentialPath + "customnamevisible")) {
									boolean tag_blockentitytag_spawnpotentials_customnamevisible = tagSection.getBoolean(spawnPotentialPath + "customnamevisible");

									// NBT BlockEntityTag.SpawnPotentials.#.Entity.CustomNameVisible
									spawnPotentialsEntity.setBoolean("CustomNameVisible", tag_blockentitytag_spawnpotentials_customnamevisible);
								}

								// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials.#.silent
								if (tagSection.contains(spawnPotentialPath + "silent")) {
									boolean tag_blockentitytag_spawnpotentials_silent = tagSection.getBoolean(spawnPotentialPath + "silent");


									// NBT BlockEntityTag.SpawnPotentials.#.Entity.Silent
									spawnPotentialsEntity.setBoolean("Silent", tag_blockentitytag_spawnpotentials_silent);
								}

								// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials.#.glowing
								if (tagSection.contains(spawnPotentialPath + "glowing")) {
									boolean tag_blockentitytag_spawnpotentials_glowing = tagSection.getBoolean(spawnPotentialPath + "glowing");


									// NBT BlockEntityTag.SpawnPotentials.#.Entity.Glowing
									spawnPotentialsEntity.setBoolean("Glowing", tag_blockentitytag_spawnpotentials_glowing);
								}

								// Store Entity to tag.BlockEntityTag.SpawnPotentials.Entity
								spawnPotentialsEntry.getCompound("data").getCompound("entity").mergeCompound(spawnPotentialsEntity);

								// TODO: Add more entity tags

							} else {
								Main.instance.getLogger().warning("Invalid value in entities.yml at entities." + cleanEntity + ".tag." + spawnPotentialPath + "entity.id. If this is a valid entity id make sure it exists in entities.yml.");
							}
						}

						// entities.yml entities.<entity>.tag.blockentitytag.spawnpotentials.#.weight
						int tag_blockentitytag_spawnpotentials_weight;
						if (tagSection.contains(spawnPotentialPath + "weight")) {
							tag_blockentitytag_spawnpotentials_weight = tagSection.getInt(spawnPotentialPath + "weight");

							// Must be greater or equal to 1.
							if (tag_blockentitytag_spawnpotentials_weight >= 1) {

								// NBT BlockEntityTag.SpawnPotentials.#.Weight
								spawnPotentialsEntry.setInteger("weight", tag_blockentitytag_spawnpotentials_weight);
							}
						}

						// Save SpawnPotentials
						blockEntityTag.getCompoundList("SpawnPotentials").addCompound().mergeCompound(spawnPotentialsEntry);
					}
				}
			}

			// entities.yml entities.<entity>.tag.spawnrange
			int tag_spawnrange_value = 4;
			if (tagSection.contains("spawnrange")) {
				int tag_spawnrange = tagSection.getInt("spawnrange");

				// Validate SpawnRange.
				if (tag_spawnrange > 0) {
					tag_spawnrange_value = tag_spawnrange;
				}
			}

			// SpawnRange
			blockEntityTag.setShort("SpawnRange", (short) tag_spawnrange_value);

			// entities.yml entities.<entity>.tag.delay
			int tag_delay_value = -1;
			if (tagSection.contains("delay")) {
				int tag_delay = tagSection.getInt("delay");

				// Validate Delay.
				if (tag_delay == -1 || tag_delay > 0) {
					tag_delay_value = tag_delay;
				}
			}

			// Delay
			blockEntityTag.setShort("Delay", (short) tag_delay_value);

			// entities.yml entities.<entity>.tag.minspawndelay
			// Note: data is set further down to check that it isn't greater than MaxSpawnDelay which depends on it.
			int tag_minspawndelay = 0;
			int tag_minspawndelay_value = 200;
			if (tagSection.contains("minspawndelay")) {
				tag_minspawndelay = tagSection.getInt("minspawndelay");
			}

			// entities.yml entities.<entity>.tag.spawncount
			// Note: MinSpawnDelay must be set.
			int tag_spawncount_value = 4;
			if (tagSection.contains("spawncount")) {
				int tag_spawncount = tagSection.getInt("spawncount");

				// Validate SpawnCount.
				if (tag_spawncount > 0 && tag_minspawndelay > 0) {
					tag_spawncount_value = tag_spawncount;
				}
			}

			// SpawnCount
			blockEntityTag.setShort("SpawnCount", (short) tag_spawncount_value);

			// entities.yml entities.<entity>.tag.maxspawndelay
			// Note: MinSpawnDelay must be set.
			int tag_maxspawndelay = 1;
			int tag_maxspawndelay_value = 800;
			if (tagSection.contains("maxspawndelay")) {
				tag_maxspawndelay = tagSection.getInt("maxspawndelay");

				// Validate MaxSpawnDelay.
				// Setting to 0 crashes Minecraft.
				if (tag_maxspawndelay > 0 && tag_minspawndelay > 0) {
					tag_maxspawndelay_value = tag_maxspawndelay;
				}
			}

			// MaxSpawnDelay
			blockEntityTag.setShort("MaxSpawnDelay", (short) tag_maxspawndelay_value);

			// Validate MinSpawnDelay.
			if (tagSection.contains("minspawndelay")) {
				if (tag_minspawndelay > 0) {

					// Can be equal to MaxSpawnDelay but not greater than.
					tag_minspawndelay_value = Math.min(tag_minspawndelay, tag_maxspawndelay);
				}
			}

			// MinSpawnDelay
			blockEntityTag.setShort("MinSpawnDelay", (short) tag_minspawndelay_value);

			// entities.yml entities.<entity>.tag.maxnearbyentities
			int tag_maxnearbyentities = 0;
			int tag_maxnearbyentities_value = 6;
			if (tagSection.contains("maxnearbyentities")) {
				tag_maxnearbyentities = tagSection.getInt("maxnearbyentities");

				// Validate MaxNearbyEntities.
				if (tag_maxnearbyentities > 0) {
					tag_maxnearbyentities_value = tag_maxnearbyentities;
				}
			}

			// MaxNearbyEntities
			blockEntityTag.setShort("MaxNearbyEntities", (short) tag_maxnearbyentities_value);

			// entities.yml entities.<entity>.tag.requiredplayerrange
			// Note: MaxNearbyEntities must be set.
			int tag_requiredplayerrange_value = 16;
			if (tagSection.contains("requiredplayerrange")) {
				int tag_requiredplayerrange = tagSection.getInt("requiredplayerrange");

				// Validate RequiredPlayerRange.
				if (tag_maxnearbyentities > 0) {
					tag_requiredplayerrange_value = tag_requiredplayerrange;
				}
			}

			// RequiredPlayerRange
			blockEntityTag.setShort("RequiredPlayerRange", (short) tag_requiredplayerrange_value);

			// Store BlockEntityTag to tag
			spawnerNBTItem.getCompound("BlockEntityTag").mergeCompound(blockEntityTag);

			// entities.yml entities.<entity>.tag.display.name
			if (tagSection.contains("display.name") && !Objects.equals(tagSection.getString("display.name"), "")) {
				String tag_display_name = Main.instance.getLangHandler().colorText(tagSection.getString("display.name"));

				String json = new Gson().toJson(tag_display_name);

				// display.Name
				NBTCompound display = spawnerNBTItem.getOrCreateCompound("display");

				display.setString("Name", json);

				spawnerNBTItem.mergeCompound(display);

			} else {
				spawner.setFormattedName(name);
			}

			spawner.setItemMeta(spawnerNBTItem.getItem().getItemMeta());
		}

		return spawner;
	}

	/**
	 * Get spawner type using NBT API plugin
	 *
	 * @return Entity Name string
	 */
	@Override
	public String getEntityNameFromSpawner(Spawner spawner) {
		if (!(spawner.getType() == Material.SPAWNER)) {
			return null;
		}

		ItemMeta meta = spawner.getItemMeta();
		if (meta == null) {
			return null;
		}

		NBTItem spawnerNBTItem = new NBTItem(spawner);

		NBTCompound blockEntityTag;

		// Check for BlockEntityTag first
		if (spawnerNBTItem.hasTag("BlockEntityTag")) {
			blockEntityTag = spawnerNBTItem.getCompound("BlockEntityTag");
		} else if (!spawnerNBTItem.hasTag("BlockEntityTag") && spawnerNBTItem.hasTag("PublicBukkitValues")) {
			// Check for any Persistent Data afterwards
			NBTCompound tag = convertPersistentDataToNBT(spawnerNBTItem);
			spawnerNBTItem.mergeCompound(tag);
			blockEntityTag = spawnerNBTItem.getCompound("BlockEntityTag");

			// Get BlockEntityTag after conversion
			if (!spawnerNBTItem.hasTag("BlockEntityTag")) {
				defaultBlockEntityTag(spawnerNBTItem);
			}
		} else {
			defaultBlockEntityTag(spawnerNBTItem);
			blockEntityTag = spawnerNBTItem.getCompound("BlockEntityTag");
		}

		NBTCompoundList spawnPotentials;
		if (!blockEntityTag.hasTag("SpawnPotentials")) {
			defaultSpawnPotentials(blockEntityTag);
		}
		spawnPotentials = blockEntityTag.getCompoundList("SpawnPotentials");

		String name = "pig";
		if (spawnPotentials.size() > 0) {
			NBTListCompound spawnPotential = spawnPotentials.get(0);
			if (spawnPotential.hasTag("data")) {
				NBTCompound data = spawnPotential.getCompound("data");
				if (data.hasTag("entity")) {
					NBTCompound entity = data.getCompound("entity");
					if (entity.hasTag("id")) {
						String nameKey = entity.getString("id");
						name = nameKey.split(":")[1];
					}
				}
			}
		} else {

			NBTCompound spawnData;
			if (!blockEntityTag.hasTag("SpawnData")) {
				defaultSpawnData(blockEntityTag);
			}
			spawnData = blockEntityTag.getCompound("SpawnData");

			if (spawnData.hasTag("entity")) {
				NBTCompound entity = spawnData.getCompound("entity");
				if (entity.hasTag("id")) {
					String nameKey = entity.getString("id");
					name = nameKey.split(":")[1];
				}
			}
		}

		return name;
	}

	/**
	 * Set spawner entity using NBT API plugin
	 */
	@Override
	public void setSpawnerEntity(Spawner spawner, String name) {
		if (!(spawner.getType() == Material.SPAWNER)) {
			return;
		}

		ItemMeta meta = spawner.getItemMeta();
		if (meta == null) {
			return;
		}

		NBTItem spawnerNBTItem = new NBTItem(spawner);

		NBTCompound blockEntityTag;

		// Check for BlockEntityTag first
		if (spawnerNBTItem.hasTag("BlockEntityTag")) {
			blockEntityTag = spawnerNBTItem.getCompound("BlockEntityTag");
		} else if (!spawnerNBTItem.hasTag("BlockEntityTag") && spawnerNBTItem.hasTag("PublicBukkitValues")) {
			// Check for any Persistent Data afterwards
			NBTCompound tag = convertPersistentDataToNBT(spawnerNBTItem);
			spawnerNBTItem.mergeCompound(tag);
			blockEntityTag = spawnerNBTItem.getCompound("BlockEntityTag");

			// Get BlockEntityTag after conversion
			if (!spawnerNBTItem.hasTag("BlockEntityTag")) {
				defaultBlockEntityTag(spawnerNBTItem);
			}
		} else {
			defaultBlockEntityTag(spawnerNBTItem);
			blockEntityTag = spawnerNBTItem.getCompound("BlockEntityTag");
		}

		// SpawnPotentials
		blockEntityTag.removeKey("SpawnPotentials");
		NBTListCompound spawnPotential = blockEntityTag.getCompoundList("SpawnPotentials").addCompound();
		spawnPotential.addCompound("data").addCompound("entity").setString("id", "minecraft:" + name);
		spawnPotential.setInteger("weight", 1);

		// SpawnData
		NBTCompound spawnData;
		if (!blockEntityTag.hasTag("SpawnData")) {
			defaultSpawnData(blockEntityTag);
		}
		spawnData = blockEntityTag.getCompound("SpawnData");

		if (spawnData.hasTag("entity")) {
			NBTCompound entity = spawnData.getCompound("entity");
			if (entity.hasTag("id")) {
				entity.setString("id", "minecraft:" + name);
			}
		}

		spawner.setItemMeta(spawnerNBTItem.getItem().getItemMeta());
	}
}